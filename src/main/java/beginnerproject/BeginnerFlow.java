package beginnerproject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class BeginnerFlow {

	@Test public void sample()  throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://www.flipkart.com");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//button[text()='✕']")).click();
	
		Actions actions=new Actions(driver);
		
		actions.moveToElement(driver.findElement(By.xpath("//span[text()='Electronics']"))).perform();
		
		driver.findElement(By.xpath("(//a[text()='Mi'])[1]")).click();
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.titleContains("Mi"));
		
		String actualtitle=driver.getTitle();
		
	    System.out.println(actualtitle);
		
		String expectedtitle="Mi Mobile Phones";
		
		if(actualtitle.contains(expectedtitle)) {
			
			System.out.println("title matching and verified");
			
		}
		else {
			
			System.out.println("title not matching");
		}
		
		driver.findElement(By.xpath("//div[text()='Newest First']")).click();
	Thread.sleep(3000);
		List<WebElement> allbrandname = driver.findElements(By.xpath("//div[@class='_3wU53n']"));
		
		for(int i=0;i<allbrandname.size();i++) {
			
			System.out.println(allbrandname.get(i).getText());
			
			
		}
		
		List<WebElement> allprice = driver.findElements(By.xpath("//div[@class='_1vC4OE _2rQ-NK']"));
		
		for(WebElement e:allprice) {
			
			System.out.println(e.getText());
		}
		
		String actualfirsttext=allbrandname.get(0).getText();
		allbrandname.get(0).click();
	   
		Thread.sleep(3000);
		Set<String> allwindows = driver.getWindowHandles();
		
		List<String> all=new ArrayList<String>(allwindows);
		
		driver.switchTo().window(all.get(1));
		
		String actualpagetitle = driver.getTitle();
		
		if(actualpagetitle.contains(actualfirsttext)) {
			
			System.out.println("title and text matching with respect to the mobile");
			
		}
		else {
			
			System.out.println("title and text not matching with respect to mobile");
		}
		
        driver.quit();	  
	   
	   /*String expectedfirsttitle ="";
	   
	   Set<String> ss = driver.getWindowHandles();
	   
	   for(String s1:ss) {
		   
		   if(!s1.equals(window1)) {
			   
			   driver.switchTo().window(s1);
			   
			   String title=driver.getTitle();
			   expectedfirsttitle=expectedfirsttitle+title;
			   
		   }
		   
		   
	   }
	   
	   if(actualfirsttitle.contains(expectedfirsttitle)) {
		   
		   System.out.println("firsttitlematching");
		   
	   }
	   
	   
		
	   
	   
		*/
		
		

	}

}
